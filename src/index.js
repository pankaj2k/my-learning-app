import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import HelloWorld from './HelloWorld';
import Method from './Method';
import Clock from './Clock';
import * as serviceWorker from './serviceWorker';



//var h1 = <h1 title = 'this is title'>hello world........................</h1>;

ReactDOM.render(

	<div>
	
		<HelloWorld link='https://www.google.com' linktext='Google' />
		<HelloWorld link='https://www.facebook.com' linktext='facebook' />
		<HelloWorld link='https://www.youtube.com' linktext='youtub' />
		<Method />

		<Clock />
	</div>
	
	, document.getElementById('root')

	);



//serviceWorker.unregister();
 