import React, { Component } from 'react';
import './App.css';

class HelloWorld extends React.Component{

  customeStyleLink = {
    backgroundColor: 'lightblue',
    textDecoration: 'underline'
  }

  render(){
    return (
      <a href ={this.props.link} style={this.customeStyleLink}>{this.props.linktext}</a>
      )
  }
}

export default HelloWorld;