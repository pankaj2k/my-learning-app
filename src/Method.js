import React, { Component } from 'react';

class Method extends Component{

	myFacebookLink(){
		return 'https://www.facebook.com/profile.php?id=1663096125'
	}

	facebookUser(){
		return 'Pankaj Kumar'
	}

	render(){
		return(
			<div>
				<p>My Facebook ID is: <a href={this.myFacebookLink()}>{this.facebookUser()}</a></p>
			</div>

			)
	}
}


export default Method;