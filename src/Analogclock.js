import React from 'react';

function Analogclock(props){
		//console.log(props.time);
		let time = new Date(props.time);
		//console.log(((getSeconds()/60)*360 - 90));

		let clockContainer = {
			position: 'relative',
			top: 0,
			left: 0,
			width: 200,
			height: 200,
			borderRadius: 20000,
			borderStyle: 'solid',
			borderColor: 'black'

		}

		let Seconds = {
			position: 'relative',
			top: 100,
			left: 100,
			border: '1px solid red',
			width: '40%',
			height: 1,
			transform: 'rotate(' + ((time.getSeconds()/60)*360 - 90).toString() + 'dag)',
			transformOrigin: '0% 0%',
			backgroundColor: 'red'

		}

		let Minutes = {
			position:'relative',
			top: 100,
			left: 100,
			border: '1px solid red',
			width: '40%',
			height: 3,
			transform: 'rotate(' + ((time.getMinutes()/60)*360 - 90).toString() + 'dag)',
			transformOrigin: '0% 0%',
			backgroundColor: 'gray'
		}

		let Hours = {
			position:'relative',
			top: 92,
			left: 106,
			border: '1px solid red',
			width: '25%',
			height: 7,
			transform: 'rotate(' + ((time.getHours()/12)*360 - 90).toString() + 'dag)',
			transformOrigin: '0% 0%',
			backgroundColor: 'gray'

		}

		return(
			<div style={clockContainer}>
				<div style={Seconds}> </div>
				<div style={Minutes}> </div>
				<div style={Hours}> </div>
			</div>
			)
}

export default Analogclock;