import React, { Component } from 'react';
import Digitalclock from './digitalclock'; // state less Component Imported
import Analogclock from './Analogclock';
import Logger from './Logger';
class Clock extends Component{

	constructor(props){
		super(props);

		this.state = {
			currentTime: new Date().toLocaleString(),
			counter: 0
		}

		this.updateTile();
	}

	updateTile(){
		setInterval(() => {
			this.setState({
				currentTime: new Date().toLocaleString(),
				counter: this.state.counter + 1
			})
		}, 1000)
	}

	render(){
		return(
			<div>
				Rendering Value From StateFull Component
				<h1>{this.state.currentTime}</h1>
				Rendering value from state Less Component
				
				<Digitalclock time ={this.state.currentTime}  /> 
				<Analogclock time = {this.state.currentTime} />

				{
					(this.state.counter < 2)?
					(<Logger time = {this.state.currentTime} />):
					(<div></div>)
				}
				

			</div>	
			)
	}
}

export default Clock;